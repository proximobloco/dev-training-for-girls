var calendario = document.querySelector('div.calendario')
var quantidadeDias = 30
var diasSelecionados = []
var texto = document.querySelector('.texto')

function criarDia(posicao) {
	var dia = document.createElement('p')
	calendario.appendChild( dia )
	dia.textContent = i
	dia.setAttribute('class', 'dia')
	dia.setAttribute('data-position', posicao)

	dia.addEventListener('click', function(){
		
		if(dia.getAttribute('class') == 'marcado'){
			dia.setAttribute('class', 'desmarcado')
			var posicaoSelecionada = 
						dia.getAttribute('data-position')
			var posicaoNaLista = 
						diasSelecionados.indexOf( posicaoSelecionada )

			diasSelecionados.splice( posicaoNaLista, 1 )
		} else {
			dia.setAttribute('class', 'marcado')
			diasSelecionados.push( dia.textContent )
		}

		if(diasSelecionados.length > 0)
			texto.textContent = diasSelecionados.join(", ")
		else 
			texto.textContent = "Nenhum dia selecionado ainda."

	})
}

for(var i=0; i<quantidadeDias; i++){
	criarDia(i)
}
